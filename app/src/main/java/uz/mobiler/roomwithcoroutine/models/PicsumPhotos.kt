package uz.mobiler.roomwithcoroutine.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PicsumPhotos(
    val author: String,
    val download_url: String,
    val height: Int,
    val id: String,
    val url: String,
    val width: Int
) {
    @PrimaryKey(autoGenerate = true)
    var idDb = 0
}