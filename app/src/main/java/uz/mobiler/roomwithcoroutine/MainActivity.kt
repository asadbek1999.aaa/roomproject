package uz.mobiler.roomwithcoroutine

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import uz.mobiler.roomwithcoroutine.adapters.RecyclerAdapter
import uz.mobiler.roomwithcoroutine.database.MyDB
import uz.mobiler.roomwithcoroutine.models.PicsumPhotos
import uz.mobiler.roomwithcoroutine.retrofit.ApiClient

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dao = MyDB.getInstance(this@MainActivity).photoDao()



        GlobalScope.launch(Dispatchers.Main) {
            val result = ApiClient().getApiClient().getPhotos()
            if (result.isSuccessful) {
                result.body()!!.forEach {
                    dao.insert(it)
                    Log.d("Shamsiddin: ", result.body().toString())
                }
            }
        }
        rv.adapter = RecyclerAdapter(dao.getPhotos(), object : RecyclerAdapter.OnLongClickListener {
            override fun onLongClick(picsumPhotos: PicsumPhotos) {
                dao.delete(picsumPhotos)
                rv.adapter?.notifyDataSetChanged()
            }

        })
        Log.d("list: ", "asadbek")
    }
}