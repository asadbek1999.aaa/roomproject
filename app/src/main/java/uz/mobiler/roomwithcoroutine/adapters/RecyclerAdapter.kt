package uz.mobiler.roomwithcoroutine.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_layout.view.*
import uz.mobiler.roomwithcoroutine.R
import uz.mobiler.roomwithcoroutine.models.PicsumPhotos

class RecyclerAdapter(var list: List<PicsumPhotos>, var listener: OnLongClickListener) :
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(picsumPhotos: PicsumPhotos) {

            Picasso.get().load(picsumPhotos.download_url).centerCrop().fit().into(itemView.image)
            itemView.tv_author.text = picsumPhotos.author
            itemView.tv_url.text = picsumPhotos.download_url
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(list[position])

        holder.itemView.setOnClickListener {
            listener.onLongClick(list[position])
        }
    }

    interface OnLongClickListener {
        fun onLongClick(picsumPhotos: PicsumPhotos)
    }
}