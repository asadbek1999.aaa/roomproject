package uz.mobiler.roomwithcoroutine.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import uz.mobiler.roomwithcoroutine.dao.PhotosDao
import uz.mobiler.roomwithcoroutine.models.PicsumPhotos

@Database(entities = [PicsumPhotos::class], version = 1, exportSchema = false)
abstract class MyDB : RoomDatabase() {

    abstract fun photoDao(): PhotosDao

    companion object {
        @Volatile
        private var INSTANCE: MyDB? = null

        fun getInstance(context: Context): MyDB {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MyDB::class.java,
                        "picsumphotos"
                    )
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}