package uz.mobiler.roomwithcoroutine.retrofit

import retrofit2.Response
import retrofit2.http.GET
import uz.mobiler.roomwithcoroutine.models.PicsumPhotos

interface ApiInterface {

    @GET("list")
    suspend fun getPhotos(): Response<List<PicsumPhotos>>
}