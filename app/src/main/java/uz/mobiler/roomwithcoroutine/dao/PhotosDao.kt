package uz.mobiler.roomwithcoroutine.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import uz.mobiler.roomwithcoroutine.models.PicsumPhotos

@Dao
interface PhotosDao {

    @Query("select * from picsumphotos")
    fun getPhotos():List<PicsumPhotos>

    @Insert
     suspend fun insert(picsumPhotos: PicsumPhotos)

    @Delete
    fun delete(picsumPhotos: PicsumPhotos)
}